package com.anaysuperprogrammist.travelrecipes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.anaysuperprogrammist.travelrecipes.dto.RecipeDTO;
import com.squareup.picasso.Picasso;


public class RecipeDetailActivity extends AppCompatActivity {

    private static final String EXTRA_Recipe = "RecipeDetailsActivity.EXTRA_Recipe";

    Toolbar toolbar;
    RecipeDTO Recipe;
    //Поля.......
    private TextView time;
    private TextView description;
    private TextView ing1;
    private TextView ing2;
    private TextView ing3;
    private ImageView image;

    public static void start(Activity caller, RecipeDTO recipe) {
        Intent intent = new Intent(caller, RecipeDetailActivity.class);
        if (recipe != null) {
            intent.putExtra(EXTRA_Recipe, recipe);
        }
        caller.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_detail);
        initRecipeDetail ();
        initToolBar();

    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(Recipe.getTitle());
    }



    public void initRecipeDetail () {
        Recipe = getIntent().getParcelableExtra(EXTRA_Recipe);

        time = findViewById(R.id.clockText);
        description = findViewById(R.id.descText);
        ing1 = findViewById(R.id.descIng1);
        ing2 = findViewById(R.id.descIng2);
        ing3 = findViewById(R.id.descIng3);
        image = findViewById(R.id.dishImage);

        time.setText(Integer.toString(Recipe.getTime()) + "мин");
        description.setText(Recipe.getDescription());
        ing1.setText(Recipe.getIngredient1());
        ing2.setText(Recipe.getIngredient2());
        ing3.setText(Recipe.getIngredient3());
        if (Recipe.getImage_url().equals("") || Recipe.getImage_url() == null) {
            image.setImageResource(R.drawable.nodishoriginal);
        } else {
            Picasso.with(this)
                    .load(Recipe.getImage_url())
                    .error(R.drawable.nodishoriginal)
                    .fit()
                    .centerCrop()
                    .into(image);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_details, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
//            case R.id.action_save:
//                if (editText.getText().length() > 0) {
//                    Recipe.text = editText.getText().toString();
//                    Recipe.done = false;
//                    Recipe.timestamp = System.currentTimeMillis();
//                    if (getIntent().hasExtra(EXTRA_Recipe)) {
//                        App.getInstance().getRecipeDao().update(Recipe);
//                    } else {
//                        App.getInstance().getRecipeDao().insert(Recipe);
//                    }
//                    finish();
//                }
//                break;
        }
        return super.onOptionsItemSelected(item);
    }
}