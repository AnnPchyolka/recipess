package com.anaysuperprogrammist.travelrecipes.dto;


import android.os.Parcel;
import android.os.Parcelable;

public class RecipeDTO implements Parcelable {
    private  Integer id;
    private  String title;
    private  Integer time;
    private  String description;
    private  String ingredient1;
    private  String ingredient2;
    private  String ingredient3;
    private String image_url;

    public RecipeDTO() {

    }

    public RecipeDTO(Integer id, String title, Integer time, String description, String ingredient1, String ingredient2, String ingredient3, String image_url) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.description = description;
        this.ingredient1 = ingredient1;
        this.ingredient2 = ingredient2;
        this.ingredient3 = ingredient3;
        this.image_url = image_url;
    }

    protected RecipeDTO(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        title = in.readString();
        if (in.readByte() == 0) {
            time = null;
        } else {
            time = in.readInt();
        }
        description = in.readString();
        ingredient1 = in.readString();
        ingredient2 = in.readString();
        ingredient3 = in.readString();
        image_url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(title);
        if (time == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(time);
        }
        dest.writeString(description);
        dest.writeString(ingredient1);
        dest.writeString(ingredient2);
        dest.writeString(ingredient3);
        dest.writeString(image_url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RecipeDTO> CREATOR = new Creator<RecipeDTO>() {
        @Override
        public RecipeDTO createFromParcel(Parcel in) {
            return new RecipeDTO(in);
        }

        @Override
        public RecipeDTO[] newArray(int size) {
            return new RecipeDTO[size];
        }
    };

    public String getImage_url() {
        return image_url;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Integer getTime() {
        return time;
    }

    public String getDescription() {
        return description;
    }

    public String getIngredient1() {
        return ingredient1;
    }

    public String getIngredient2() {
        return ingredient2;
    }

    public String getIngredient3() {
        return ingredient3;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIngredient1(String ingredient1) {
        this.ingredient1 = ingredient1;
    }

    public void setIngredient2(String ingredient2) {
        this.ingredient2 = ingredient2;
    }

    public void setIngredient3(String ingredient3) {
        this.ingredient3 = ingredient3;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }


}
