package com.anaysuperprogrammist.travelrecipes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


import android.widget.Toast;

import com.anaysuperprogrammist.travelrecipes.adapter.RecipeListAdapter;
import com.anaysuperprogrammist.travelrecipes.dto.RecipeDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;



public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("nlohmann_json");
    }


    private Button updateButton;
    private ProgressBar progressBar;
    private boolean fail = true;
    private Toast toast;
    private Toolbar toolbar;
    private RecyclerView rv;
    private RecipeListAdapter adapter;
    private GETRequest task;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new RecipeListAdapter();
        setContentView(R.layout.activity_main);
        toast = Toast.makeText(getApplicationContext(), "Привет!", Toast.LENGTH_SHORT);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        updateButton = findViewById(R.id.updateButton);
//        updateButton.setVisibility(View.INVISIBLE);

        initToolBar();
        rv = findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rv.setAdapter(adapter);
        tryingToRecieve();

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateButton.setEnabled(false);
                    tryingToRecieve();
                }
            });


    }


    private void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        toolbar.inflateMenu(R.menu.menu);
    }


    public void refreshData() {
        adapter.notifyDataSetChanged();
    }


    //  Проверка интернет соединения
    public static boolean isOnline(Context context)
    {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        return false;
    }



    // Toast customize
    public void createCustomToast(String message) {
        toast.setText(message);
        View view = toast.getView();
        view.setBackgroundColor(Color.GRAY);
        TextView text = (TextView) view.findViewById(android.R.id.message);
        text.setTextColor(Color.WHITE);
        toast.setGravity(Gravity.CENTER,0,0);
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        updateButton.setVisibility(Button.VISIBLE);
        updateButton.setEnabled(true);
        toast.show();
    }


    public void tryingToRecieve() {
        progressBar.setVisibility(ProgressBar.VISIBLE);
        if (isOnline(getApplicationContext())) {
                task = new GETRequest();
                task.execute();
        } else {
            updateButton.setEnabled(true);
            createCustomToast("Нет интернет соединения!");
        }
    }

    private TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return originalTrustManager.getAcceptedIssuers();
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        try {
                            originalTrustManager.checkClientTrusted(certs, authType);
                        } catch (CertificateException ignored) {
                        }
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        try {
                            originalTrustManager.checkServerTrusted(certs, authType);
                        } catch (CertificateException ignored) {
                        }
                    }
                }
        };
    }



    private SSLSocketFactory getSSLSocketFactory() {
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = getResources().openRawResource(R.raw.baeldung);
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
            } finally {
                caInput.close();
            }

            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, getWrappedTrustManagers(tmf.getTrustManagers()), null);

            return sslContext.getSocketFactory();
        } catch (Exception e) {
            createCustomToast("Небезопасное соединение");
            return HttpsURLConnection.getDefaultSSLSocketFactory();
        }
    }

    private class GETRequest extends AsyncTask<Void, Void, List<RecipeDTO>> {
        @Override
        protected List<RecipeDTO> doInBackground(Void... params) {
            List<RecipeDTO> list = new ArrayList<>();
            try {
                URL url = new URL(Constants.URL.GET_RECIPE_ITEM);
                //String token = "8d014abb-d328-42d7-a4a4-696f7825d593";
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setSSLSocketFactory(getSSLSocketFactory());
                urlConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
//                        HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
//                        boolean t = hv.verify(Constants.URL.GET_RECIPE_ITEM, session);
                        return true;
                    }
                });
//                urlConnection.setRequestProperty("Authorization", "Bearer " + token);
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                print_https_cert(urlConnection);
                System.out.println(urlConnection.getResponseCode());
                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String data ="";

                    InputStream inputStream = urlConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(inputStream)));
                    String line = "";
                    while (line != null) {
                        line = bufferedReader.readLine();
                        data = data + line;
                    }
                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();
                    JSONArray JA = new JSONArray(data);
                    for (int i=0; i<JA.length(); ++i) {
                        JSONObject o = (JSONObject) JA.get(i);
                        RecipeDTO res = new Gson().fromJson(o.toString(), RecipeDTO.class);
                        list.add(res);
                    }

                }
                return list;
            } catch (Exception e) {
                this.cancel(true);
                return list;
            }
        }

        @Override
        protected void onPostExecute(List<RecipeDTO> list) {
              updateButton.setVisibility(View.INVISIBLE);
              progressBar.setVisibility(View.INVISIBLE);
              adapter.setList(list);
              refreshData();
        }

        @Override
        public void onCancelled() {
            createCustomToast("Не удалось свяваться с сервером");
        }
    }





    // Вывод сертификата
    public void print_https_cert(HttpsURLConnection con) {

        if (con != null) {

            try {
                System.out.println("Response Code : " + con.getResponseCode());
                System.out.println("Cipher Suite : " + con.getCipherSuite());
                System.out.println("\n");

                Certificate[] certs = con.getServerCertificates();
                for (Certificate cert : certs) {
                    System.out.println("Cert Type : " + cert.getType());
                    System.out.println("Cert Hash Code : " + cert.hashCode());
                    System.out.println("Cert Public Key Algorithm : "
                            + cert.getPublicKey().getAlgorithm());
                    System.out.println("Cert Public Key Format : "
                            + cert.getPublicKey().getFormat());
                    System.out.println("\n");
                }

            } catch (SSLPeerUnverifiedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


