package com.anaysuperprogrammist.travelrecipes.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.anaysuperprogrammist.travelrecipes.R;
import com.anaysuperprogrammist.travelrecipes.RecipeDetailActivity;
import com.anaysuperprogrammist.travelrecipes.dto.RecipeDTO;
import com.squareup.picasso.Picasso;

import java.text.CollationElementIterator;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.RecipeViewHolder>{

    private List<RecipeDTO> list;


    public static class RecipeViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView title;
        TextView time;
        ImageView image;
        TextView ing1;
        TextView ing2;
        TextView ing3;
        RecipeDTO recipe;


        public RecipeViewHolder(@NonNull final View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            title = itemView.findViewById(R.id.title);
            time = itemView.findViewById(R.id.time);
            ing1 = itemView.findViewById(R.id.ing1);
            ing2 = itemView.findViewById(R.id.ing2);
            ing3 = itemView.findViewById(R.id.ing3);
            image = itemView.findViewById(R.id.dishImage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RecipeDetailActivity.start((Activity) itemView.getContext(), recipe);
                }
            });
        }

        public void bind(RecipeDTO item) {
            this.recipe = item;
            title.setText(item.getTitle());
            time.setText("Готовить " + (String.valueOf(item.getTime())) + " мин");
            ing1.setText(item.getIngredient1());
            ing2.setText(item.getIngredient2());
            ing3.setText(item.getIngredient3());
            if (item.getImage_url().equals("") || item.getImage_url() == null ) {
                image.setImageResource(R.drawable.nodishoriginal);
            } else {
                Picasso.with(itemView.getContext())
                        .load(item.getImage_url())

                        .error(R.drawable.nodishoriginal)
                        .fit()
                        .centerCrop()
                        .into(image);
            }
        }

    }



    public RecipeListAdapter() {
        this.list = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecipeViewHolder (
                LayoutInflater.from(parent.getContext()).inflate(R.layout.recipe_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder holder, int position) {
        holder.bind(list.get(position));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }



    public void setList(List<RecipeDTO> list) {
        this.list = list;
    }
}
