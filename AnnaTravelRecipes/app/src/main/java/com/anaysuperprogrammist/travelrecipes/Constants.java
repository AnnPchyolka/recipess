package com.anaysuperprogrammist.travelrecipes;

public class Constants {
    public static class URL {
        private static final String HOST = "https://192.168.0.104:8081/";
        private static final String HOST_MOSCOW = "https://172.17.5.188:8081/";

        public static final String GET_RECIPE_ITEM = HOST_MOSCOW + "api/public/recipes";
    }
}
