import axios from 'axios'
import Utils from '../utils/Utils'
import {alertActions, store} from "../utils/Rdx";

const API_URL = 'https://localhost:8081/api/tr'
const AUTH_URL = 'https://localhost:8081/auth'

class BackendService {
 /* Auth */
    login(login, password) {
        return axios.post(`${AUTH_URL}/login`, {login, password})
    }

    logout() {
        /* return axios.get (`${AUTH_URL}/logout`, { headers: {Authorization: Utils.getToken() }}) */
        return axios.get (`${AUTH_URL}/logout` )
    }


/* Recipes */

    retrieveAllRecipes(page,limit) {
            return axios.get(`${API_URL}/recipes?page=${page}&limit=${limit}`); //?page=${page}&limit=${limit}`
        }

    retrieveRecipe(id) {
        return axios.get(`${API_URL}/recipes/${id}`);
    }

    createRecipe(recipe) {
        return axios.post(`${API_URL}/recipes`, recipe);
    }

    updateRecipe(recipe) {
        return axios.put(`${API_URL}/recipes/${recipe.id}`, recipe);
    }

    deleteRecipes(recipes) {
        return axios.post(`${API_URL}/deleterecipes`, recipes);
    }
}


function showError(msg)
{
    store.dispatch(alertActions.error(msg))
}

axios.interceptors.request.use(
    config => {
        store.dispatch(alertActions.clear())
        let token = Utils.getToken();
        if (token)
            config.headers.Authorization = token;
        return config;
    },
    error => {
        showError(error.message)
        return Promise.reject(error);
    })

axios.interceptors.response.use(undefined,
    error => {
        if (error.response && error.response.status && [401, 403].indexOf(error.response.status) !== -1)
            showError("Ошибка авторизации")
        else if (error.response && error.response.data && error.response.data.message)
            showError(error.response.data.message)
        else
            showError(error.message)
        return Promise.reject(error);
    })

export default new BackendService()