import React, { Component } from 'react';
import BackendService from '../services/BackendService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faChevronLeft, faSave} from '@fortawesome/free-solid-svg-icons'
import {alertActions} from "../utils/Rdx"
import {connect} from "react-redux"
import Alert from './Alert'
import {Form} from "react-bootstrap"

class RecipeComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            title: this.props.match.params.image_url,
            time: 45,
            description: '',
            ingredient1: '',
            ingredient2: '',
            ingredient3: '',
            image_url: '',
            hidden: false,
            show_alert: false,
            message: '',
        }

        this.onSubmit = this.onSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.closeAlert = this.closeAlert.bind(this)
    }

    handleChange({ target}) {
        this.setState({ [target.name]: target.value });
    };

closeAlert()
    {
        this.setState({ show_alert : false })
    }

    onSubmit(event) {
        event.preventDefault();
        event.stopPropagation();

        let err = null;
        if (!parseInt(this.state.time)) {
                err = "В поле \"время\" должно быть число!"
                 this.setState({ show_alert : true , message : err})
                 return
                }
        if (!this.state.title || this.state.time == '' || !this.state.description || !this.state.ingredient1 || !this.state.ingredient2 || !this.state.ingredient3) {
            err = "Пожалуйста, заполните все поля!"
            this.setState({ show_alert : true , message : err})
            return
        }


        let recipe = { id: this.state.id,
                        title: this.state.title,
                         time: this.state.time,
                         description: this.state.description,
                         ingredient1: this.state.ingredient1,
                         ingredient2: this.state.ingredient2,
                         ingredient3: this.state.ingredient3,
                         image_url: this.state.image_url
                         }
        if (parseInt(recipe.id) === -1) {
            BackendService.createRecipe(recipe)
                .then(()=> this.props.history.push('/recipes'))
                .catch(()=>{})
        }
        else {
            BackendService.updateRecipe(recipe)
                .then(()=> this.props.history.push('/recipes'))
                .catch(()=>{})
        }
    }

    componentDidMount() {
        if (parseInt(this.state.id) !== -1) {
            BackendService.retrieveRecipe(this.state.id)
                .then((resp) => {
                    this.setState({
                        title: resp.data.title,
                        time: resp.data.time,
                        description: resp.data.description,
                        ingredient1: resp.data.ingredient1,
                        ingredient2: resp.data.ingredient2,
                        ingredient3: resp.data.ingredient3,
                        image_url: resp.data.image_url
                    })
                })
                .catch(()=> this.setState({ hidden: true }))
        }
    }

    render() {
        if (this.state.hidden)
            return null;
        return (
            <div className="m-4">
                <div className="row my-2 mr-0">
                    <h3>Рецепт</h3>
                    <button
                        className="btn btn-outline-secondary ml-auto"
                        onClick={() => this.props.history.goBack()}><FontAwesomeIcon
                        icon={faChevronLeft}/>{' '}Назад</button>
                </div>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group>
                        <Form.Label>Название</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Введите название блюда"
                            onChange={this.handleChange}
                            value={this.state.title}
                            name="title"
                            autoComplete="off"
                        />
                        <hr></hr>
                        <Form.Label>Примерное время на приговление</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Введите кол-во минут"
                            onChange={this.handleChange}
                            value={this.state.time}
                            name="time"
                            autoComplete="off"
                        />
                        <br></br>
                        <Form.Label>Описание</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Введите этапы приготовления"
                            onChange={this.handleChange}
                            value={this.state.description}
                            name="description"
                            autoComplete="off"
                        />


                        <br></br>
                        <Form.Label>Ингридиенты</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Введите 1 ингридиент"
                            onChange={this.handleChange}
                            value={this.state.ingredient1}
                            name="ingredient1"
                            autoComplete="off"
                        />
                        <br></br>

                        <Form.Control
                            type="text"
                            placeholder="Введите 2 ингридиент"
                            onChange={this.handleChange}
                            value={this.state.ingredient2}
                            name="ingredient2"
                            autoComplete="off"
                        />
                        <br></br>

                        <Form.Control
                            type="text"
                            placeholder="Введите 3 ингридиент"
                            onChange={this.handleChange}
                            value={this.state.ingredient3}
                            name="ingredient3"
                            autoComplete="off"
                        />
                        <br></br>
                        <Form.Label>Изображение</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Введите ссылку на изображение блюда"
                            onChange={this.handleChange}
                            value={this.state.image_url}
                            name="image_url"
                            autoComplete="off"
                        />
                    </Form.Group>
                    <button
                        className="btn btn-outline-secondary"
                        type="submit"><FontAwesomeIcon
                        icon={faSave}/>{' '}Сохранить</button>
                </Form>
                <Alert
                                    title="Предупреждение"
                                    message={this.state.message}
                                    ok={this.onDelete}
                                    close={this.closeAlert}
                                    modal={this.state.show_alert}
                                    cancelButton={true}
                                />
            </div>
        )
    }
}

export default connect()(RecipeComponent);


