import React from 'react';
import { Link } from 'react-router-dom'
import { Nav } from 'react-bootstrap'
import { faSun } from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


class SideBar extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
            { this.props.expanded &&
                <Nav className={"flex-column my-sidebar my-sidebar-expanded"}>
                    <Nav.Item><Nav.Link as={Link} to="/recipes"><FontAwesomeIcon icon={faSun}/>{' '}Рецепты</Nav.Link></Nav.Item>
                </Nav>
            }
            { !this.props.expanded &&
                <Nav className={"flex-column my-sidebar my-sidebar-collapsed"}>
                    <Nav.Item><Nav.Link as={Link} to="/recipes"><FontAwesomeIcon icon={faSun} size="2x"/></Nav.Link></Nav.Item>
                </Nav>
            }
            </>
        );
    }
}

export default SideBar;