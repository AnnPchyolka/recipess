import React, {useState} from 'react';
import './App.css';
import {Router, Switch, Route, Redirect } from "react-router-dom";
import { createBrowserHistory } from 'history';
import NavigationBar from './components/NavigationBar'
import SideBar from './components/SideBar'
import Home from "./components/Home";
import Login from "./components/Login";
import Utils from "./utils/Utils"
import {connect} from "react-redux";
import RecipeListComponent from "./components/RecipeListComponent";
import RecipeComponent from "./components/RecipeComponent";


const AuthRoute = props => {
    let user = Utils.getUser();
    if (!user) return <Redirect to="/login" />;
    return <Route {...props} />;
};

const history = createBrowserHistory();


function App(props) {
    const [exp, setExpanded] = useState(false);

    return (
    <div className="App">
        <Router history = { history }>
            <NavigationBar toggleSideBar={()=> setExpanded(!exp)}/>
            <div className="wrapper">
                <SideBar expanded={exp}/>
                <div className="container-fluid">
                    {props.error_message &&
                    <div className="alert alert-danger m-1">{props.error_message}</div>
                    }
                    <Switch>
                        <AuthRoute path="/home" component={Home}/>
                        <AuthRoute path="/recipes" exact component={RecipeListComponent}/>
                        <AuthRoute path="/recipes/:id" component={RecipeComponent}/>
                        <Route path="/login" component={Login} />
                    </Switch>
                </div>
            </div>
      </Router>
    </div>
  );
}

function mapStateToProps(state) {
    const { msg } = state.alert;
    return { error_message: msg };
}

export default connect(mapStateToProps)(App);

