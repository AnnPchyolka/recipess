package com.travelrecipes.backendTR.controllers;

import com.travelrecipes.backendTR.tools.DataValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.travelrecipes.backendTR.models.Recipe;
import com.travelrecipes.backendTR.repositories.RecipeRepository;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("api/tr")
public class RecipeController {
    @Autowired
    RecipeRepository recipeRepository;

    @GetMapping("/recipes")
    public Page<Recipe> getAllCountries(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        return recipeRepository.findAll(PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "title")));
    }

    @GetMapping("/recipes/{id}")
    public ResponseEntity<Recipe> getRecipe(@PathVariable(value = "id") Long recipeId)
            throws DataValidationException
    {
        Recipe recipe = recipeRepository.findById(recipeId)
                .orElseThrow(()-> new DataValidationException("Рецепт с таким индексом не найден"));
        return ResponseEntity.ok(recipe);
    }

    @PostMapping("/recipes")
    public ResponseEntity<Object> createRecipe(@Valid @RequestBody Recipe recipe)
            throws DataValidationException {
        try {
            Recipe nc = recipeRepository.save(recipe);
            return new ResponseEntity<Object>(nc, HttpStatus.OK);
        }
        catch(Exception ex)
        {
            String error;
            if (ex.getMessage().contains("recipes.name_UNIQUE"))
                throw new DataValidationException("Эта страна уже есть в базе");
            else
                throw new DataValidationException("Пожалуйста, заполните все поля!");
        }
    }

//    @GetMapping("/recipes/{id}/artists")
//    public ResponseEntity<List<Artist>> getRecipeArtists(@PathVariable(value = "id") Long RecipeId) {
//        Optional<Recipe> cc = RecipeRepository.findById(RecipeId);
//        if (cc.isPresent()) {
//            return ResponseEntity.ok(cc.get().artists);
//        }
//        return ResponseEntity.ok(new ArrayList<Artist>());
//    }

    @PutMapping("/recipes/{id}")
    public ResponseEntity<Recipe> updateRecipe(@PathVariable(value = "id") Long recipeId,
                                                 @Valid @RequestBody Recipe recipeDetails)
            throws DataValidationException
    {
        try {
            Recipe recipe = recipeRepository.findById(recipeId)
                    .orElseThrow(() -> new DataValidationException("Страна с таким индексом не найдена"));
            recipe.title = recipeDetails.title;
            recipe.time = recipeDetails.time;
            recipe.description = recipeDetails.description;
            recipe.ingredient1 = recipeDetails.ingredient1;
            recipe.ingredient2 = recipeDetails.ingredient2;
            recipe.ingredient3 = recipeDetails.ingredient3;
            recipe.image_url = recipeDetails.image_url;
            recipeRepository.save(recipe);
            return ResponseEntity.ok(recipe);
        }
        catch (Exception ex) {
            String error;
            if (ex.getMessage().contains("recipes.name_UNIQUE"))
                throw new DataValidationException("Этот рецепт уже есть в базе");
            else
                throw new DataValidationException("Неизвестная ошибка");
        }
    }

    @PostMapping("/deleterecipes")
    public ResponseEntity deleteRecipes(@Valid @RequestBody List<Recipe> recipes) {
        recipeRepository.deleteAll(recipes);
        return new ResponseEntity(HttpStatus.OK);
    }

}
