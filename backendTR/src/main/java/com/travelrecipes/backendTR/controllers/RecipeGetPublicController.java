package com.travelrecipes.backendTR.controllers;

import com.travelrecipes.backendTR.models.Recipe;
import com.travelrecipes.backendTR.repositories.RecipeRepository;
import com.travelrecipes.backendTR.tools.DataValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/public")
public class RecipeGetPublicController {
    @Autowired
    RecipeRepository recipeRepository;

    @GetMapping("/recipes")
    public List<Recipe> getAllCountries() {
        return recipeRepository.findAll(Sort.by(Sort.Direction.ASC, "title"));
    }

}
