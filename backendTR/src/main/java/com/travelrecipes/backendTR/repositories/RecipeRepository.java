package com.travelrecipes.backendTR.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.travelrecipes.backendTR.models.Recipe;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {

}

