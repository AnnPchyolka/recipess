package com.travelrecipes.backendTR.models;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.annotation.processing.Generated;
import javax.persistence.*;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "recipes")
@Access(AccessType.FIELD)
public class Recipe {

    public Recipe() { }
    public Recipe(int id) { this.id = id; }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    public long id;

    @Column(name = "title", nullable = false, unique = true)
    public String title;

    @Column(name = "time", nullable = false)
    public Integer time;

    @Column(name = "description", nullable = false)
    public String description;

    @Column(name = "ingredient1", nullable = false)
    public String ingredient1;
    @Column(name = "ingredient2", nullable = false)
    public String ingredient2;
    @Column(name = "ingredient3", nullable = false)
    public String ingredient3;

    @Column(name = "image_url", nullable = true)
    public String image_url;

}
