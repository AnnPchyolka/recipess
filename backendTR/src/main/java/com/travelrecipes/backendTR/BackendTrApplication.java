package com.travelrecipes.backendTR;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendTrApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendTrApplication.class, args);
	}

}
